<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Drupal Region'),
  'description' => t('Renders the blocks in a region.'),
  'category' => t('Page Elements'),
  'edit form' => 'panels_region_ctools_region_edit_form',
  'render callback' => 'panels_region_ctools_region_render',
  'admin info' => 'panels_region_ctools_region_admin_info',
  'defaults' => array(
      'region' => '',
  )
);